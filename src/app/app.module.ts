import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import {DatePipe} from '@angular/common';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './components/header/header.component';
import { ContainerComponent } from './components/container/container.component';
import { RoomComponent } from './components/room/room.component';
import { DeviceComponent } from './components/device/device.component';
import { FormComponent } from './components/form/form.component';
import { SettingsMenuComponent } from './components/settings/settingsMenu/settingsMenu.component'
import { SettingsHistoricComponent } from './components/settings/settingsHistoric/settingsHistoric.component';
import { SettingsComponent } from './components/settings/settings.component';
import { SettingsDevicesComponent } from './components/settings/settingsDevices/settingsDevices.component';
import { SettingsStatsComponent } from './components/settings/settingsStats/settingsStats.component';
import { InfoComponent } from './components/info/info.component';

import { registerLocaleData } from '@angular/common';
import localeFr from '@angular/common/locales/fr';
import { InfoMenuComponent } from "./components/info/info-menu/info-menu.component";
import { MenuStatsComponent } from './components/info/menu-stats/menu-stats.component';
import { MenuInfosComponent } from './components/info/menu-infos/menu-infos.component';
import { MenuHistComponent } from './components/info/menu-hist/menu-hist.component';
import { ChartsModule } from 'ng2-charts';


// the second parameter 'fr' is optional
registerLocaleData(localeFr, 'fr');

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    ContainerComponent,
    RoomComponent,
    FormComponent,
    DeviceComponent,
    SettingsMenuComponent,
    SettingsComponent,
    SettingsHistoricComponent,
    SettingsDevicesComponent,
    SettingsStatsComponent,
    InfoComponent,
    InfoMenuComponent,
    MenuStatsComponent,
    MenuInfosComponent,
    MenuHistComponent,
    MenuInfosComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    ChartsModule
  ],
  providers: [DatePipe],
  bootstrap: [AppComponent]
})
export class AppModule { }
