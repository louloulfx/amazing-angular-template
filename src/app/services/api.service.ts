import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Device } from '../device.model';
import { Room } from '../room.model';
import { Observable, throwError } from 'rxjs';
import { retry, catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ApiService {
  apiURL = 'http://localhost:3000';
  constructor(private http: HttpClient) { }

  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': 'my-auth-token'
    })
  }

  getDevices(): Observable<Device> {
    return this.http.get<Device>(this.apiURL + '/device')
      .pipe(
        retry(1),
        catchError(this.handleError)
      )
  }
  getDevice(_id): Observable<Device> {
    return this.http.get<Device>(this.apiURL + '/device/' + _id)
      .pipe(
        retry(1),
        catchError(this.handleError)
      )
  }
  updateDevice(_id, device): Observable<Device> {
    return this.http.put<Device>(this.apiURL + '/device/' + _id, JSON.stringify(device), this.httpOptions)
      .pipe(
        retry(1),
        catchError(this.handleError)
      )
  }
  deleteDevice(_id) {
    return this.http.delete<Device>(this.apiURL + '/device/' + _id, this.httpOptions)
      .pipe(
        retry(1),
        catchError(this.handleError)
      )
  }

  getRooms(): Observable<Room> {
    return this.http.get<Room>(this.apiURL + '/room')
      .pipe(
        retry(1),
        catchError(this.handleError)
      )
  }

  handleError(error) {
    let errorMessage = '';
    if (error.error instanceof ErrorEvent) {
      // Get client-side error
      errorMessage = error.error.message;
    } else {
      // Get server-side error
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    window.alert(errorMessage);
    return throwError(errorMessage);
  }
}

