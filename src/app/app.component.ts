import { Component, ViewEncapsulation, Renderer2 } from '@angular/core';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss'],
    encapsulation: ViewEncapsulation.None
})
export class AppComponent {
    storedTheme: string = localStorage.getItem("theme");
    constructor(private renderer: Renderer2) {
        
        if (this.storedTheme == null) {
            localStorage.setItem("theme", 'white');
        }
        this.renderer.addClass(document.body, localStorage.getItem("theme"));
    }

}
