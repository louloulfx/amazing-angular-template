import { Component, OnInit } from '@angular/core';
import { ApiService } from "../../services/api.service";
import { ActivatedRoute, Router } from '@angular/router';
import { Device } from 'src/app/device.model';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.scss']
})
export class FormComponent implements OnInit {
  id = this.actRoute.snapshot.params['id'];
  deviceData: any = {};
  name: String;
  room: String;
  state: boolean;
  historiqueOn: [];
  historiqueOff: [];

  constructor(
    public restApi: ApiService,
    public actRoute: ActivatedRoute,
    public router: Router,
    private datePipe: DatePipe
  ) {
  }
  public medium;
  ngOnInit() {
    this.restApi.getDevice(this.id).subscribe((data: {}) => {
      this.deviceData = data;
    })
  }

  date = this.datePipe.transform(new Date(), "dd/MM/yyyy hh:mm:ss");
  trueorfalse() {
    if (this.deviceData.state === true) {
      this.deviceData.state = false 
      this.deviceData.historique.push(this.date + " " + this.deviceData.name + " dans " + this.deviceData.room + " éteinds") 
      this.restApi.updateDevice(this.id, this.deviceData).subscribe(data => {
        
      })
    } else {
      this.deviceData.state = true
      this.deviceData.historique.push(this.date + " " + this.deviceData.name + " dans " + this.deviceData.room + " allumé") 
      this.restApi.updateDevice(this.id, this.deviceData).subscribe(data => {

      })
    }
  }

  updateDevice() {
    if (window.confirm('Are you sure, you want to update?')) {
      this.restApi.updateDevice(this.id, this.deviceData).subscribe(data => {
        this.router.navigate(['/device'])
      })
    }
  }

  deleteDevice() {
    if (window.confirm('Are you sure, you want to delete?')) {
      this.restApi.deleteDevice(this.id).subscribe(data => {
        this.router.navigate(['/device'])
      })
    }
  }
}
