import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MenuHistComponent } from './menu-hist.component';

describe('MenuHistComponent', () => {
  let component: MenuHistComponent;
  let fixture: ComponentFixture<MenuHistComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MenuHistComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MenuHistComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
