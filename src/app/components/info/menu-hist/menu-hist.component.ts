import { Component, OnInit } from '@angular/core';
import { ApiService } from "../../../services/api.service";
import { ActivatedRoute, Router } from '@angular/router';
import { Device } from 'src/app/device.model';

@Component({
  selector: 'app-menu-hist',
  templateUrl: './menu-hist.component.html',
  styleUrls: ['./menu-hist.component.scss']
})
export class MenuHistComponent implements OnInit {
  parametres: string;
  url = document.URL;
  id = this.url.slice(27, 51);
  deviceData: any = {};
  room: String;
  storedTheme

  constructor(
    public restApi: ApiService,
    public actRoute: ActivatedRoute,
    public router: Router
  ) {
  }

  ngOnInit() {
    console.log(this.id);
    this.restApi.getDevice(this.id).subscribe((data: {}) => {
      this.deviceData = data;
    })
    this.storedTheme = localStorage.getItem("theme")
  }


}
