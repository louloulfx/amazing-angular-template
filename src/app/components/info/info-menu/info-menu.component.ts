import { Component, OnInit } from '@angular/core';
import { ApiService } from '../../../services/api.service';
@Component({
    selector: 'app-info-menu',
    templateUrl: './info-menu.component.html',
    styleUrls: ['./info-menu.component.scss']
})
export class InfoMenuComponent implements OnInit {
    storedTheme
    constructor() { }
    ngOnInit() {
        this.storedTheme = localStorage.getItem("theme");
    }
}
