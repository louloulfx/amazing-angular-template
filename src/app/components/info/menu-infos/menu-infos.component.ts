import { Component, OnInit } from '@angular/core';
import { ApiService } from "../../../services/api.service";
import { ActivatedRoute, Router } from '@angular/router';
import { Device } from 'src/app/device.model';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'app-menu-infos',
  templateUrl: './menu-infos.component.html',
  styleUrls: ['./menu-infos.component.scss']
})
export class MenuInfosComponent implements OnInit {
  id = this.actRoute.snapshot.params['id'];
  deviceData: any = {};
  date: string;
  storedTheme
  constructor(
    public restApi: ApiService,
    public actRoute: ActivatedRoute,
    public router: Router,
    private datePipe: DatePipe
  ) {
    setInterval(() => {
      this.date = this.datePipe.transform(new Date(), "dd/MM/yyyy hh:mm:ss");
    }, 1);
  }

  ngOnInit() {
    console.log(this.id)
    this.restApi.getDevice(this.id).subscribe((data: {}) => {
      this.deviceData = data;
    })

    this.storedTheme = localStorage.getItem("theme");
  }


  trueorfalse() {
    if (this.deviceData.state === true) {
      this.deviceData.state = false
      this.deviceData.historique.push(this.date + " " + this.deviceData.name + " dans " + this.deviceData.room + " éteinds")
      this.restApi.updateDevice(this.id, this.deviceData).subscribe(data => {

      })
    } else {
      this.deviceData.state = true
      this.deviceData.historique.push(this.date + " " + this.deviceData.name + " dans " + this.deviceData.room + " allumé")
      this.restApi.updateDevice(this.id, this.deviceData).subscribe(data => {

      })
    }
  }

  updateDevice() {
    if (window.confirm('Are you sure, you want to update?')) {
      this.restApi.updateDevice(this.id, this.deviceData).subscribe(data => {
        window.location.reload(true);
      })
    }
  }

  deleteDevice() {
    if (window.confirm('Are you sure, you want to delete?')) {
      this.restApi.deleteDevice(this.id).subscribe(data => {
        this.router.navigate(['/device'])
      })
    }
  }

}
