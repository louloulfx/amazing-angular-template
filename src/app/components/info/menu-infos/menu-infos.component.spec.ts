import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MenuInfosComponent } from './menu-infos.component';

describe('MenuInfosComponent', () => {
  let component: MenuInfosComponent;
  let fixture: ComponentFixture<MenuInfosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MenuInfosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MenuInfosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
