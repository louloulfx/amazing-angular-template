import { Component, OnInit } from '@angular/core';
import { ApiService } from '../../services/api.service'; 

@Component({
  selector: 'app-room',
  templateUrl: './room.component.html',
  styleUrls: ['./room.component.scss'],
})

export class RoomComponent implements OnInit {
  Device: any = [];
  room: string = '';
  Rooms: any = []
  storedTheme
  constructor(public restApi: ApiService
  ) { }

  ngOnInit() {
    this.loadDevice();
    this.loadRoom();
    this.storedTheme = localStorage.getItem("theme");
  }
  loadRoom() {
    return this.restApi.getRooms().subscribe((data: {}) => {
      this.Rooms = data;
    })
  }

  loadDevice() {
    return this.restApi.getDevices().subscribe((data: {}) => {
      this.Device = data;
    })
  }

  getDeviceByRoom(room: string){
    return this.Device.filter(device => device.room == room);
  }
}
