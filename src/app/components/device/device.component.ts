import { Component, OnInit, Input, HostListener } from '@angular/core';
import { ApiService } from '../../services/api.service';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'app-device',
  templateUrl: './device.component.html',
  styleUrls: ['./device.component.scss']
})
export class DeviceComponent implements OnInit {
  Device: any = [];
  oneDevice: any;
  date: string;
  stateclass: string;
  @Input() name: string;
  @Input() id: string;
  @Input() state: boolean;

  @HostListener("click") onClick() {
    this.hide()
  }
  storedTheme
  constructor(
    public restApi: ApiService,
    private datePipe: DatePipe) {
    setInterval(() => {
      this.date = this.datePipe.transform(new Date(), "dd/MM/yyyy hh:mm:ss");
    }, 1);
  }

  shown: boolean = false;
  clicked: boolean = false;

  ngOnInit() {
    this.loadDevice()
    this.storedTheme = localStorage.getItem("theme");
    this.showstate()
  }

  loadDevice() {
    return this.restApi.getDevices().subscribe((data: {}) => {
      this.Device = data;
    })
  }

  show() {
    if (!this.shown) {
      this.shown = true;
      this.clicked = true;

    } else {
      this.shown = false;
    }
  }

  deleteDevice(_id) {
    if (window.confirm('Are you sure, you want to delete?')) {
      this.restApi.deleteDevice(_id).subscribe(data => {
        this.loadDevice()
        window.location.reload(true);
      })
    }
  }

  hide() {
    if (this.shown && !this.clicked) {
      this.shown = false;
      console.log(this.shown)
    } else {
      this.clicked = false;
    }
  }

  showstate() {
    if (this.state) {
      this.stateclass = "activated"
    } else {
      this.stateclass = ""
    }
  }

  trueorfalse(_id: string) {
    this.oneDevice = this.Device.filter(x => x._id == _id)[0];
    if (this.oneDevice.state) {
      this.oneDevice.state = false
      this.oneDevice.historique.push(this.date + " " + this.oneDevice.name + " dans " + this.oneDevice.room + " éteint")
      this.restApi.updateDevice(_id, this.oneDevice).subscribe(data => {
        this.loadDevice()
        window.location.reload(true);
      })
    } else {
      this.oneDevice.state = true
      this.oneDevice.historique.push(this.date + " " + this.oneDevice.name + " dans " + this.oneDevice.room + " allumé")
      this.restApi.updateDevice(_id, this.oneDevice).subscribe(data => {
        this.loadDevice()
        window.location.reload(true);
      })
    }
  }


}

