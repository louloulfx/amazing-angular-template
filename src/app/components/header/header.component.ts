import { Component, OnInit } from '@angular/core';
import { Router } from "@angular/router";
import { Location } from "@angular/common";
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  results = '';
  public currentRoute;
  public isHome: boolean;
  public now: Date = new Date();
  showLoader: boolean = true;
  storedTheme;

  constructor(public location: Location, public router: Router, private http: HttpClient) {
    setInterval(() => {
      this.now = new Date();
    }, 1);

    router.events.subscribe(val => {
      if (location.path() != "") {
        this.isHome = false;
      } else {
        this.isHome = true;
      }
    });
  }

  ngOnInit() {
    this.storedTheme = localStorage.getItem("theme");

  }
}
