import { Component, OnInit } from '@angular/core';
import { ApiService } from '../../../services/api.service';
import { DatePipe } from '@angular/common';
import { Device } from 'src/app/device.model';
@Component({
  selector: 'app-settings-devices',
  templateUrl: './settingsDevices.component.html',
  styleUrls: ['./settingsDevices.component.scss']
})
export class SettingsDevicesComponent implements OnInit {
  Device: any = [];
  name: String;
  room: String;
  state: boolean;
  oneDevice: any;
  date: string;
  storedTheme;

  constructor(public restApi: ApiService,
    private datePipe: DatePipe) {
    setInterval(() => {
      this.date = this.datePipe.transform(new Date(), "dd/MM/yyyy hh:mm:ss");
    }, 1);
  }

  ngOnInit() {
    this.loadDevice()
    this.storedTheme = localStorage.getItem("theme");
  }

  loadDevice() {
    return this.restApi.getDevices().subscribe((data: {}) => {
      this.Device = data;
    })
  }

  deleteDevice(_id) {
    if (window.confirm('Are you sure, you want to delete?')) {
      this.restApi.deleteDevice(_id).subscribe(data => {
        this.loadDevice()
      })
    }
  }

  trueorfalse(_id: string) {
    this.oneDevice = this.Device.filter(x => x._id == _id)[0];
    if (this.oneDevice.state) {
      this.oneDevice.state = false
      this.oneDevice.historique.push(this.date + " " + this.oneDevice.name + " dans " + this.oneDevice.room + " éteint")
      this.restApi.updateDevice(_id, this.oneDevice).subscribe(data => {
        this.loadDevice()
      })
    } else {
      this.oneDevice.state = true
      this.oneDevice.historique.push(this.date + " " + this.oneDevice.name + " dans " + this.oneDevice.room + " allumé")
      this.restApi.updateDevice(_id, this.oneDevice).subscribe(data => {
        this.loadDevice()
      })
    }
  }
}
