import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-settings-menu',
  templateUrl: './settingsMenu.component.html',
  styleUrls: ['./settingsMenu.component.scss']
})
export class SettingsMenuComponent implements OnInit {
  constructor() { }
  storedTheme
  ngOnInit() {
    this.storedTheme = localStorage.getItem("theme");
  }

}
