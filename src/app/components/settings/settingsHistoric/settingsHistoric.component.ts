import { Component, OnInit } from '@angular/core';
import { ApiService } from '../../../services/api.service';

@Component({
  selector: 'app-settings-historic',
  templateUrl: './settingsHistoric.component.html',
  styleUrls: ['./settingsHistoric.component.scss']
})
export class SettingsHistoricComponent implements OnInit {
  Device: any = [];
  showLoader: boolean = true;
  theme = "theme";
  storedTheme;

  constructor(public restApi: ApiService) {}

  ngOnInit() {
    this.loadDevice()
    this.storedTheme = localStorage.getItem("theme");
  }

  loadDevice() {
    return this.restApi.getDevices().subscribe((data: {}) => {
      this.Device = data;
      this.showLoader = false;
    })
  }

  setTheme() {
    if (this.storedTheme == "white") {
      localStorage.setItem(this.theme, 'dark');
    } else {
      localStorage.setItem(this.theme, 'white');
    }
    window.location.reload(true);
  }

}
