export class Device {
    _id: string;
    name: string;
    state: boolean;
    type: string;
    room: string;
    historique: Array<String>;
}
