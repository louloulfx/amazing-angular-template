import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DeviceComponent } from './components/device/device.component';
import { RoomComponent } from './components/room/room.component';
import { FormComponent } from './components/form/form.component';
import { SettingsComponent } from './components/settings/settings.component';
import { SettingsHistoricComponent } from './components/settings/settingsHistoric/settingsHistoric.component';
import { SettingsDevicesComponent } from './components/settings/settingsDevices/settingsDevices.component';
import { SettingsStatsComponent } from './components/settings/settingsStats/settingsStats.component';
import { InfoComponent } from './components/info/info.component';
import { InfoMenuComponent } from "./components/info/info-menu/info-menu.component";
import { SettingsMenuComponent } from './components/settings/settingsMenu/settingsMenu.component';
import { MenuInfosComponent } from './components/info/menu-infos/menu-infos.component';
import { MenuStatsComponent } from './components/info/menu-stats/menu-stats.component';
import { MenuHistComponent } from './components/info/menu-hist/menu-hist.component';

const routes: Routes = [
  { path: 'device', component: DeviceComponent },
  { path: '', component: RoomComponent },
  { path: 'device-settings/:id', component: FormComponent },

  {
    path: 'settings', component: SettingsComponent, children: [
      { path: '', component: SettingsHistoricComponent },
      { path: 'devices', component: SettingsDevicesComponent },
      { path: 'stats', component: SettingsStatsComponent }
    ]
  },
  { path: 'settings-menu', component: SettingsMenuComponent },

  {
    path: 'info/:id', component: InfoComponent, children: [
      { path: '', component: MenuInfosComponent },
      { path: 'stats', component: MenuStatsComponent },
      { path: 'hist', component: MenuHistComponent },
      
    ]
  },
  { path: 'info-menu', component: InfoMenuComponent },

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
